  <section>
    
    <!-- I. PERSONAL INFORMATION -->

    <hr class="m-t-12 m-b-10">
    <h2 class="box-title text-info"><b>I. Personal Information</b></h2>
    
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="CSIDNO">CS ID NO:</label>
                <input type="text" name="CSIDNO" id="CSIDNO" class="form-control"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
             <div class="form-group">
                <label for="SURNAME">Last Name:</label>
                <input type="text" name="SURNAME" id="SURNAME" class="form-control">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="FIRSTNAME">First Name:</label>
                <input type="text" name="FIRSTNAME" id="FIRSTNAME" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="MIDDLENAME">Middle Name:</label>
                <input type="text" name="MIDDLENAME" id="MIDDLENAME" class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="NAMEEXT">Extension Name:</label>
                 <input type="text" name="NAMEEXT" id="NAMEEXT" class="form-control">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="DATEOFBIRTH">Date of Birth:</label>
                <input type="date" class="form-control" id="DATEOFBIRTH" name="DATEOFBIRTH"> 
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="PLACEOFBIRTH">Place of Birth:</label>
                 <input type="text" name="PLACEOFBIRTH" id="PLACEOFBIRTH" class="form-control">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="SEX">Sex:</label>
                <div class="col-md-12">
                    <input name="SEX" type="radio" id="Male" value="Male" class="radio-col-red"/>
                    <label for="Male">Male &emsp;</label>

                    <input name="SEX" type="radio" id="Female" value="Female" class="radio-col-red"/>
                    <label for="Female">Female</label>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="form-group">
                <label for="CIVILSTATUS">Civil Status:</label>
                <div class="col-md-12">
                    <input type="radio" id="Single" value="Single" name="CIVILSTATUS" class="radio-col-red"/>
                    <label for="Single">Single &emsp;</label>

                    <input type="radio" id="Married" value="Married" name="CIVILSTATUS" class="radio-col-red"/>
                    <label for="Married">Married &emsp;</label>

                    <input type="radio" id="Widowed" value="Widowed" name="CIVILSTATUS" class="radio-col-red"/>
                    <label for="Widowed">Widowed &emsp;</label>

                    <input type="radio" id="Separated" value="Separated" name="CIVILSTATUS" class="radio-col-red"/>
                    <label for="Separated">Separated &emsp;</label>

                    <input type="radio" id="Others" value="Others" name="CIVILSTATUS" class="radio-col-red"/>
                    <label for="Others">Others</label>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
       <div class="col-md-4">
            <div class="form-group">
                <label for="Height">Height:</label>
                <input type="text" name="HEIGHT" id="Height" class="form-control">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="Weight">Weight:</label>
                <input type="text" name="WEIGHT" id="Weight" class="form-control">
            </div>
        </div>
        <div class="col-md-4">
             <div class="form-group">
                <label for="BloodType">Blood Type:</label>
                <input type="text" name="BLOODTYPE" id="BloodType" class="form-control">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="GSIS">GSIS ID No:</label>
                 <input type="text" name="GSIS" id="GSIS" class="form-control">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="PAGIBIG">Pag-Ibig ID No:</label>
                <input type="text" name="PAGIBIG" id="PAGIBIG" class="form-control">
            </div>
        </div>
        
        <div class="col-md-4">
            <div class="form-group">
                <label for="PHILHEALTH">PhilHealth No:</label>
                <input type="text" name="PHILHEALTH" id="PHILHEALTH" class="form-control">
            </div>
        </div>
    </div>
        
    <div class="row">
        <div class="col-md-4">
             <div class="form-group">
                <label for="SSS">SSS No:</label>
                <input type="text" name="SSS" id="SSS" class="form-control">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="TIN">TIN No:</label>
                 <input type="text" name="TIN" id="TIN" class="form-control">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="AGENCYEMPNO">Agency Employee No:</label>
                 <input type="text" name="AGENCYEMPNO" id="AGENCYEMPNO" class="form-control">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="CITIZENSHIP">Citizenship:</label>
                <select class="custom-select form-control" id="CITIZENSHIP" name="CITIZENSHIP">
                    <option value="">Please Select</option>
                    <option value="Filipino">Filipino</option>
                    <option value="Dual Citizenship">Dual Citizenship</option>
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="EMAIL">Email Address</label>
                 <input type="email" name="EMAIL" id="EMAIL" class="form-control">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="RESADDRESS">Residential Address</label>
                <textarea class="form-control" rows="4" id="RESADDRESS" name="RESADDRESS"></textarea>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="PERADDRESS">Permanent Address</label>
                <textarea class="form-control" rows="4" id="PERADDRESS" name="PERADDRESS"></textarea>
            </div>
        </div>
    </div>

    <div class="row">
         <div class="col-md-6">
            <div class="form-group">
                <label for="TELNO">Telephone Number</label>
                 <input type="text" name="TELNO" id="TELNO" class="form-control">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="MOBNO">Cellphone Number</label>
                 <input type="text" name="MOBNO" id="MOBNO" class="form-control">
            </div>
        </div>
    </div>

    <!-- II. FAMILY BACKGROUND -->
    
    <hr class="m-t-12 m-b-10">
    <h2 class="box-title text-info"><b>II. Family Background</b></h2>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="SPOUSESURNAME">Spouse's Surname:</label>
                <input type="text" name="SPOUSESURNAME" id="SPOUSESURNAME" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="SPOUSEFIRSTNAME">First Name:</label>
                <input type="text" name="SPOUSEFIRSTNAME" id="SPOUSEFIRSTNAME" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
             <div class="form-group">
                <label for="SPOUSEMIDDLENAME">Middle Name:</label>
                <input type="text" name="SPOUSEMIDDLENAME" id="SPOUSEMIDDLENAME" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="SPOUSENAMEEXT">Extension Name :</label>
                 <input type="text" name="SPOUSENAMEEXT" id="SPOUSENAMEEXT" class="form-control">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="SPOUSEOCC">Occupation:</label>
                <input type="text" name="SPOUSEOCC" id="SPOUSEOCC" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="SPOUSEBUSNAME">Employer/Business Name:</label>
                <input type="text" name="SPOUSEBUSNAME" id="SPOUSEBUSNAME" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="SPOUSEBUSADDRESS">Business Address:</label>
                <input type="text" name="SPOUSEBUSADDRESS" id="SPOUSEBUSADDRESS" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="SPOUSEBUSTEL">Business Telephone No:</label>
                <input type="text" name="SPOUSEBUSTEL" id="SPOUSEBUSTEL" class="form-control">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="FATHERSURNAME">Father's Surname:</label>
                <input type="text" name="FATHERSURNAME" id="FATHERSURNAME" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="FATHERFIRSTNAME">First Name:</label>
                <input type="text" name="FATHERFIRSTNAME" id="FATHERFIRSTNAME" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
             <div class="form-group">
                <label for="FATHERMIDDLENAME">Middle Name:</label>
                <input type="text" name="FATHERMIDDLENAME" id="FATHERMIDDLENAME" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="FATHERNAMEEXT">Extension Name :</label>
                 <input type="text" name="FATHERNAMEEXT" id="FATHERNAMEEXT" class="form-control">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="MOTHERMAIDENNAME">Mother's Maiden Name:</label>
                <input type="text" name="MOTHERMAIDENNAME" id="MOTHERMAIDENNAME" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="MOTHERSURNAME">Surname:</label>
                <input type="text" name="MOTHERSURNAME" id="MOTHERSURNAME" class="form-control">
            </div>
        </div>
                <div class="col-md-3">
            <div class="form-group">
                <label for="MOTHERFIRSTNAME">First Name :</label>
                 <input type="text" name="MOTHERFIRSTNAME" id="MOTHERFIRSTNAME" class="form-control">
            </div>
        </div>
        <div class="col-md-3">
             <div class="form-group">
                <label for="MOTHERMIDDLENAME">Middle Name:</label>
                <input type="text" name="MOTHERMIDDLENAME" id="MOTHERMIDDLENAME" class="form-control">
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <table class="table table-bordered color-table info-table">
                <thead>
                    <tr>
                        <th class="text-left" rowspan="2">
                            <label for="CHILDNAME">Children</label>
                        </th>
                        <th>
                            <label for="CHILDDOB">Date of Birth:</label>
                        </th>
                        <th>
                            <button type="button" id="addChildren" class="btn btn-xs btn-default">
                                <i class="fa fa-plus"></i>
                            </button>
                        </th>
                    </tr>
                </thead>
                <script type="text/javascript">
                $(function() {
                    $('#addChildren').click(function() {
                        childAdd();
                    });

                    $('body').delegate('.removes', 'click', function() {
                        $(this).parent().parent().remove();
                    });

                });

                function childAdd() {
                    var n = ($('.children tr').length - 0) + 1;
                    var tr = '<tr>' +
                        '<td> <input type="text" name="CHILDNAME" id="CHILDNAME" placeholder="Children Name" class="form-control"></td>' +
                        '<td> <input type="date" class="form-control" id="CHILDDOB" name="CHILDDOB"> </td>' +
                        '<td><button type="button" class="removes btn btn-xs btn-warning"><i class="fa fa-minus"></i></button></td>' +
                        '</tr>';
                    $('.children').append(tr);
                }
                </script>
                <tbody class="children">
                    <tr>
                        <td>
                            <input type="text" name="CHILDNAME" id="CHILDNAME" placeholder="Children Name" class="form-control">
                            <!-- <input type="text" class="form-control civil_dateExam" name="civil_dateExam[]"> -->
                        </td>
                        <td>
                            <input type="date" class="form-control" id="CHILDDOB" name="CHILDDOB"> 
                        </td>
                        <td>
                            <button type="button" class="removes btn btn-xs btn-warning"><i class="fa fa-minus"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>


    <!-- III. EDUCATIONAL BACKGROUND -->

    <hr class="m-t-12 m-b-10">
    <h2 class="box-title text-info"><b>III. Educational Background</b></h2>
    
    <div class="row">
        <div class="col-12 m-t-20">
            <div class="card">
                <div class="card-header card-info">
                    <b style="color:#ffffff;">Elementary School</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="ELEMSCHOOL">Elementary School Name:</label>
                                <input type="text" name="ELEMSCHOOL" id="ELEMSCHOOL" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="ELEMBASICEDUC">Basic Education/ Degree/ Course:</label>
                                <input type="text" name="ELEMBASICEDUC" id="ELEMBASICEDUC" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="ELEMYEARGRADUATED">Year Graduated:</label>
                                <input type="date" name="ELEMYEARGRADUATED" id="ELEMYEARGRADUATED" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="ELEMATTENDANCE">Period Attendance:</label>
                                <input type="date" class="form-control" id="ELEMATTENDANCE" name="ELEMATTENDANCE"> 
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="ELEMHIGHESTLEVEL">Highest level/ Units earned:</label>
                                <input type="text" class="form-control" id="ELEMHIGHESTLEVEL" name="ELEMHIGHESTLEVEL"> 
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="ELEMSCHOLARSHIP">Scholarship/ Academic Honors:</label>
                                <input type="text" name="ELEMSCHOLARSHIP" id="ELEMSCHOLARSHIP" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>

    <div class="row">
        <div class="col-12 m-t-20">
            <div class="card">
                <div class="card-header card-info">
                    <b style="color:#ffffff;">High School</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="HSSCHOOL">High School Name:</label>
                                <input type="text" name="HSSCHOOL" id="HSSCHOOL" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="HSBASICEDUC">Basic Education/ Degree/ Course:</label>
                                <input type="text" name="HSBASICEDUC" id="HSBASICEDUC" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="HSYEARGRADUATED">Year Graduated:</label>
                                <input type="date" name="HSYEARGRADUATED" id="HSYEARGRADUATED" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="HSATTENDANCE">Period Attendance:</label>
                                <input type="date" class="form-control" id="HSATTENDANCE" name="HSATTENDANCE"> 
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="HSHIGHESTLEVEL">Highest level/ Units earned:</label>
                                <input type="text" class="form-control" id="HSHIGHESTLEVEL" name="HSHIGHESTLEVEL"> 
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="HSSCHOLARSHIP">Scholarship/ Academic Honors:</label>
                                <input type="text" name="HSSCHOLARSHIP" id="HSSCHOLARSHIP" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    
    <div class="row">
        <div class="col-12 m-t-20">
            <div class="card">
                <div class="card-header card-info">
                    <b style="color:#ffffff;">Vocational/ Trade Course</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="VOCSCHOOL">School Name:</label>
                                <input type="text" name="VOCSCHOOL" id="VOCSCHOOL" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="VOCBASICEDUC">Basic Education/ Degree/ Course:</label>
                                <input type="text" name="VOCBASICEDUC" id="VOCBASICEDUC" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="VOCYEARGRADUATED">Year Graduated:</label>
                                <input type="date" name="VOCYEARGRADUATED" id="VOCYEARGRADUATED" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="VOCATTENDANCE">Period Attendance:</label>
                                <input type="date" class="form-control" id="VOCATTENDANCE" name="VOCATTENDANCE"> 
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="VOCHIGHESTLEVEL">Highest level/ Units earned:</label>
                                <input type="text" class="form-control" id="VOCHIGHESTLEVEL" name="VOCHIGHESTLEVEL"> 
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="VOCSCHOLARSHIP">Scholarship/ Academic Honors:</label>
                                <input type="text" name="VOCSCHOLARSHIP" id="VOCSCHOLARSHIP" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>

    <div class="row">
        <div class="col-12 m-t-20">
            <div class="card">
                <div class="card-header card-info">
                    <b style="color:#ffffff;">College School</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="COLSCHOOL">School Name:</label>
                                <input type="text" name="COLSCHOOL" id="COLSCHOOL" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="COLBASICEDUC">Basic Education/ Degree/ Course:</label>
                                <input type="text" name="COLBASICEDUC" id="COLBASICEDUC" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="COLYEARGRADUATED">Year Graduated:</label>
                                <input type="date" name="COLYEARGRADUATED" id="COLYEARGRADUATED" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="COLATTENDANCE">Period Attendance:</label>
                                <input type="date" class="form-control" id="COLATTENDANCE" name="COLATTENDANCE"> 
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="COLHIGHESTLEVEL">Highest level/ Units earned:</label>
                                <input type="text" class="form-control" id="COLHIGHESTLEVEL" name="COLHIGHESTLEVEL"> 
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="COLSCHOLARSHIP">Scholarship/ Academic Honors:</label>
                                <input type="text" name="COLSCHOLARSHIP" id="COLSCHOLARSHIP" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    
    <div class="row">
        <div class="col-12 m-t-20">
            <div class="card">
                <div class="card-header card-info">
                    <b style="color:#ffffff;">Graduate Studies</b>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="GSSCHOOL">School Name:</label>
                                <input type="text" name="GSSCHOOL" id="GSSCHOOL" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="GSBASICEDUC">Basic Education/ Degree/ Course:</label>
                                <input type="text" name="GSBASICEDUC" id="GSBASICEDUC" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="GSYEARGRADUATED">Year Graduated:</label>
                                <input type="date" name="GSYEARGRADUATED" id="GSYEARGRADUATED" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="GSATTENDANCE">Period Attendance:</label>
                                <input type="date" class="form-control" id="GSATTENDANCE" name="GSATTENDANCE"> 
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="GSHIGHESTLEVEL">Highest level/ Units earned:</label>
                                <input type="text" class="form-control" id="GSHIGHESTLEVEL" name="GSHIGHESTLEVEL"> 
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="GSSCHOLARSHIP">Scholarship/ Academic Honors:</label>
                                <input type="text" name="GSSCHOLARSHIP" id="GSSCHOLARSHIP" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
</section>