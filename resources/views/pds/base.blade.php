@extends('layouts.app-template')

@section('content')

<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Personal Data Sheet</h3>
    </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Personal Data Sheet</li>
        </ol>
    </div>
   <!--  <div>
        <a href="{{url('pds-records/create')}}" title="Add Record" class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-plus text-white"></i></a>
    </div> -->
</div>

@yield('action-content')

@endsection 