@extends('pds.base')

@section('action-content')

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-3 pull-right">
                        <a href="{{ route('pds-records.create') }}" class="btn btn-lg waves-effect waves-light btn-block btn-info col-lg-12 col-md-4">Add Record</a>
                    </div>
                    <div class="col-md-9">
                    <h4 class="card-title">All Records</h4>
                    <h6 class="card-subtitle">Description <a href="http://www.csc.gov.ph" target="_blank"><i>CSC</i></a></h6>
                    </div>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>PDS No</th>
                                    <th>LAST NAME</th>
                                    <th>FIRST NAME</th>
                                    <th>MIDDLE INITIAL</th>
                                    <th>EXT NAME</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($pds as $value)
                            <tr>
                                <td>{{$value -> CSIDNO}}</td>
                                <td>{{$value -> SURNAME}}</td>
                                <td>{{$value -> FIRSTNAME}}</td>
                                <td>{{$value -> MIDDLENAME}}</td>
                                <td>{{$value -> NAMEEXT}}</td>
                                <td>
                                    <a href="" class="btn btn-xs btn-info">Update</a>
                                    <a href="" class="btn btn-xs btn-primary">View</a>
                                    <a href="" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach                           
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 

@endsection