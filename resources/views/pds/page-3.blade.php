<section>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="intType1">Interview Type :</label>
                <select class="custom-select form-control" id="intType1" data-placeholder="Type to search cities" name="intType1">
                    <option value="normal">Normal</option>
                    <option value="difficult">Difficult</option>
                    <option value="hard">Hard</option>
                </select>
            </div>
            <div class="form-group">
                <label for="Location1">Location :</label>
                <select class="custom-select form-control" id="Location1" name="location">
                    <option value="">Select City</option>
                    <option value="India">India</option>
                    <option value="USA">USA</option>
                    <option value="Dubai">Dubai</option>
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="jobTitle2">Interview Date :</label>
                <input type="date" class="form-control" id="jobTitle2" name="idate">
            </div>
            <div class="form-group">
                <label>Requirements :</label>
                <div class="c-inputs-stacked">
                    <label class="inline custom-control custom-checkbox block">
                        <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description ml-0">Employee</span> 
                    </label>
                    <label class="inline custom-control custom-checkbox block">
                        <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description ml-0">Contract</span> 
                    </label>
                </div>
            </div>
        </div>
    </div>
</section>