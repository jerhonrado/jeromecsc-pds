<section>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="participants1">Confidance</label>
                <input type="text" class="form-control" id="participants1" name="con">
            </div>
            <div class="form-group">
                <label for="participants1">Result</label>
                <select class="custom-select form-control" id="participants1" name="res">
                    <option value="">Select Result</option>
                    <option value="Selected">Selected</option>
                    <option value="Rejected">Rejected</option>
                    <option value="Call Second-time">Call Second-time</option>
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="decisions1">Comments</label>
                <textarea name="decisions" id="decisions1" rows="4" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <label>Rate Interviwer :</label>
                <div class="c-inputs-stacked">
                    <label class="inline custom-control custom-checkbox block">
                        <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description ml-0">1 star</span> 
                    </label>
                    <label class="inline custom-control custom-checkbox block">
                        <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description ml-0">2 star</span> 
                    </label>
                    <label class="inline custom-control custom-checkbox block">
                        <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description ml-0">3 star</span> 
                    </label>
                    <label class="inline custom-control custom-checkbox block">
                        <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description ml-0">4 star</span> 
                    </label>
                    <label class="inline custom-control custom-checkbox block">
                        <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description ml-0">5 star</span> 
                    </label>
                </div>
            </div>
        </div>
    </div>
</section>