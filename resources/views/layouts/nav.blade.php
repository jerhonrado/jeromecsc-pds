<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile">
            <!-- User profile image -->
            <div class="profile-img"> <img src="{{asset('/assets/images/users/user-1.png')}}" alt="user" /> 
                     <!-- this is blinking heartbit-->
                    <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
            </div>
            <!-- User profile text-->
            <div class="profile-text"> 
                    <h5>{{ Auth::user()->name }}</h5>
                    <a href="" class="" data-toggle="tooltip" title="Profile"><i class="mdi mdi-account-card-details"></i></a>
                    <a href="" class="" data-toggle="tooltip" title="Settings"><i class="mdi mdi-settings"></i></a>
                    <a href="{{ route('logout') }}" title="Logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="mdi mdi-power"></i></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
            </div>
        </div>
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul>
                <li class="nav-devider"></li>

                <li> <a href="\home" aria-expanded="false"><i class="mdi mdi-chart-timeline"></i><span class="hide-menu">Dashboard</span></a>
                </li>

                <li> <a href="{{url('pds-records')}}" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu">Personal Data Sheet</span></a>
                </li>

                <li> <a href="javascript:void(0);" aria-expanded="false"><i class="mdi mdi-file-export"></i><span class="hide-menu">Export Records</span></a>

                <li> <a href="javascript:void(0);" aria-expanded="false"><i class="mdi mdi-printer"></i><span class="hide-menu">Reports</span></a>
                </li>

                <li> <a href="{{url('manage-users')}}" aria-expanded="false"><i class="mdi mdi-account-key"></i><span class="hide-menu">Manage Users</span></a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
