@extends('user.base')

@section('action-content')
<div class="container-fluid">
	<div class="row">
	    <div class="col-12">
	        <div class="card">
	            <!-- .left-right-aside-column-->
	            <div class="contact-page-aside">
	                <!-- .left-aside-column-->
	                <div class="left-aside bg-light-part">
	                    <ul class="list-style-none">
	                        <li class="box-label"><a href="javascript:void(0)">All Users <span>1</span></a></li>
	                        <li class="divider"></li>
	                        <li><a href="javascript:void(0)">Administrator <span>0</span></a></li>
	                        <li><a href="javascript:void(0)">User <span>0</span></a></li>
	                        <li><a href="javascript:void(0)">Guest <span>0</span></a></li>
	                    </ul>
	                </div>
	                <!-- /.left-aside-column-->
	                <div class="right-aside ">
                    <div class="col-12">
                    <div class="col-md-3 pull-right">
                        <a href="{{ route('manage-users.create') }}" class="btn btn-lg waves-effect waves-light btn-block btn-info col-lg-12 col-md-4">Add User</a>
                    </div>
                    <div class="col-md-9">
                    <h4 class="card-title">List of Administrator and Users</h4>
                    <h6 class="card-subtitle">Description <a href="http://www.csc.gov.ph" target="_blank"><i>CSC</i></a></h6>
                    </div>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Role</th>
                                    <th>Email</th>
                                    <th>Date Registered</th>
                                    <th>Action</th>
                                </tr>
                            </thead> 
                            <tbody>
                            @foreach($user as $users)
                            <tr>
                                <td>{{ $users->name }}</td>
                                <td>Administrator</td>
                                <td>{{ $users->email }}</td>
                                <td>{{ date('F d, Y', strtotime($users->created_at)) }}</td>
                                <td>
                                    <a href="" class="btn btn-xs btn-info">Update</a>
                                    <a href="" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    </div>
	                </div>
	                <!-- /.left-right-aside-column-->
	            </div>
	        </div>
	    </div>
	</div>
</div>
@endsection