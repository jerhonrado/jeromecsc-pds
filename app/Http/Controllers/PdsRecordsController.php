<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use App\PdsRecords;

class PdsRecordsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         $this->middleware('auth');
    } 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $pds = DB::table('pds_records')->get();
        return view('/pds.index', ['pds' => $pds]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/pds/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $keys = ['CSIDNO', 'SURNAME', 'FIRSTNAME', 'MIDDLENAME', 'NAMEEXT', 'DATEOFBIRTH', 'PLACEOFBIRTH', 'SEX', 'CIVILSTATUS', 'HEIGHT', 'WEIGHT', 'BLOODTYPE', 'GSIS', 'PAGIBIG', 'PHILHEALTH', 'SSS', 'TIN', 'AGENCYEMPNO', 'CITIZENSHIP', 'RESADDRESS', 'PERADDRESS', 'TELNO', 'MOBNO', 'EMAIL', 'SPOUSESURNAME', 'SPOUSEFIRSTNAME', 'SPOUSEMIDDLENAME', 'SPOUSENAMEEXT', 'SPOUSEOCC', 'SPOUSEBUSNAME', 'SPOUSEBUSADDRESS', 'SPOUSEBUSTEL',  'CHILDNAME',  'CHILDDOB',  'FATHERSURNAME',  'FATHERFIRSTNAME',  'FATHERMIDDLENAME',  'FATHERNAMEEXT',  'MOTHERMAIDENNAME',  'MOTHERSURNAME',  'MOTHERFIRSTNAME',  'MOTHERMIDDLENAME', 'ELEMSCHOOL',  'ELEMBASICEDUC',  'ELEMATTENDANCE',  'ELEMHIGHESTLEVEL',  'ELEMYEARGRADUATED',  'ELEMSCHOLARSHIP', 'HSSCHOOL',  'HSBASICEDUC',  'HSATTENDANCE',  'HSHIGHESTLEVEL',  'HSYEARGRADUATED',  'HSSCHOLARSHIP', 'VOCSCHOOL',  'VOCBASICEDUC',  'VOCATTENDANCE',  'VOCHIGHESTLEVEL',  'VOCYEARGRADUATED',  'VOCSCHOLARSHIP', 'COLSCHOOL',  'COLBASICEDUC',  'COLATTENDANCE',  'COLHIGHESTLEVEL',  'COLYEARGRADUATED',  'COLSCHOLARSHIP', 'GSSCHOOL',  'GSBASICEDUC',  'GSATTENDANCE',  'GSHIGHESTLEVEL',  'GSYEARGRADUATED',  'GSSCHOLARSHIP'];
        
        $input = $this->createQueryInput($keys, $request);
        PdsRecords::create($input);

        return redirect()->intended('/pds-records/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    private function createQueryInput($keys, $request) {
        $queryInput = [];
        for($i = 0; $i < sizeof($keys); $i++) {
            $key = $keys[$i];
            $queryInput[$key] = $request[$key];
        }

        return $queryInput;
    }

}
