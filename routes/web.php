<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// LOGIN AND REG
Route::get('/', function () {
    return view('auth.login');
});

// Login and Registration
Auth::routes();

// HOME
Route::get('/home', 'HomeController@index')->name('home');

// PDS RECORDS
Route::resource('pds-records', 'PdsRecordsController'); //FOR CRUD
Route::get('/pds-records', function () { //DISPLAY
     $pds = DB::table('pds_records')->get();
     // $pds = DB::table('pds_records')->paginate(12);
     return view('pds.index', ['pds' => $pds]);
});

// USER MANAGEMENT
Route::resource('manage-users', 'ManageUsersController'); //FOR CRUD
Route::get('/manage-users', function() {
	$user = DB::table('users')->get();
	return view('user.index', ['user' => $user]);
});