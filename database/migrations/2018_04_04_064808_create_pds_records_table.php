<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePdsRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pds_records', function (Blueprint $table) {
            
            // I. PERSONAL INFORMATION
            $table->increments('ID');
            $table->string('CSIDNO')->unique()->nullable();
            $table->string('SURNAME')->nullable();
            $table->string('FIRSTNAME')->nullable();
            $table->string('MIDDLENAME')->nullable();
            $table->string('NAMEEXT')->nullable();
            $table->date('DATEOFBIRTH')->nullable();
            $table->string('PLACEOFBIRTH')->nullable();
            $table->string('SEX')->nullable();
            $table->string('CIVILSTATUS')->nullable();
            $table->string('HEIGHT')->nullable();
            $table->string('WEIGHT')->nullable();
            $table->string('BLOODTYPE')->nullable();
            $table->string('GSIS')->nullable();
            $table->string('PAGIBIG')->nullable();
            $table->string('PHILHEALTH')->nullable();
            $table->string('SSS')->nullable();
            $table->string('TIN')->nullable();
            $table->string('AGENCYEMPNO')->nullable();
            $table->string('CITIZENSHIP')->nullable();
            $table->string('RESADDRESS')->nullable();
            $table->string('PERADDRESS')->nullable();
            $table->string('TELNO')->nullable();
            $table->string('MOBNO')->nullable();
            $table->string('EMAIL')->nullable();
            
            // II. FAMILY BACKGROUND
            $table->string('SPOUSESURNAME')->nullable();
            $table->string('SPOUSEFIRSTNAME')->nullable();
            $table->string('SPOUSEMIDDLENAME')->nullable();
            $table->string('SPOUSENAMEEXT')->nullable();
            $table->string('SPOUSEOCC')->nullable();
            $table->string('SPOUSEBUSNAME')->nullable();
            $table->string('SPOUSEBUSADDRESS')->nullable();
            $table->string('SPOUSEBUSTEL')->nullable();
            $table->string('CHILDNAME')->nullable();
            $table->string('CHILDDOB')->nullable();
            $table->string('FATHERSURNAME')->nullable();
            $table->string('FATHERFIRSTNAME')->nullable();
            $table->string('FATHERMIDDLENAME')->nullable();
            $table->string('FATHERNAMEEXT')->nullable();
            $table->string('MOTHERMAIDENNAME')->nullable();
            $table->string('MOTHERSURNAME')->nullable();
            $table->string('MOTHERFIRSTNAME')->nullable();
            $table->string('MOTHERMIDDLENAME')->nullable();

            // III. EDUCATIONAL BACKGROUND
            $table->string('ELEMSCHOOL')->nullable();
            $table->string('ELEMBASICEDUC')->nullable();
            $table->date('ELEMATTENDANCE')->nullable();
            $table->string('ELEMHIGHESTLEVEL')->nullable();
            $table->date('ELEMYEARGRADUATED')->nullable();
            $table->string('ELEMSCHOLARSHIP')->nullable();

            $table->string('HSSCHOOL')->nullable();
            $table->string('HSBASICEDUC')->nullable();
            $table->date('HSATTENDANCE')->nullable();
            $table->string('HSHIGHESTLEVEL')->nullable();
            $table->date('HSYEARGRADUATED')->nullable();
            $table->string('HSSCHOLARSHIP')->nullable();

            $table->string('VOCSCHOOL')->nullable();
            $table->string('VOCBASICEDUC')->nullable();
            $table->date('VOCATTENDANCE')->nullable();
            $table->string('VOCHIGHESTLEVEL')->nullable();
            $table->date('VOCYEARGRADUATED')->nullable();
            $table->string('VOCSCHOLARSHIP')->nullable();

            $table->string('COLSCHOOL')->nullable();
            $table->string('COLBASICEDUC')->nullable();
            $table->date('COLATTENDANCE')->nullable();
            $table->string('COLHIGHESTLEVEL')->nullable();
            $table->date('COLYEARGRADUATED')->nullable();
            $table->string('COLSCHOLARSHIP')->nullable();

            $table->string('GSSCHOOL')->nullable();
            $table->string('GSBASICEDUC')->nullable();
            $table->date('GSATTENDANCE')->nullable();
            $table->string('GSHIGHESTLEVEL')->nullable();
            $table->date('GSYEARGRADUATED')->nullable();
            $table->string('GSSCHOLARSHIP')->nullable();
            $table->timestamps();
        });
    }

            // $table->string('')->nullable();


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pds_records');
    }
}
